<!DOCTYPE html>
<html lang="en">
<!--
 webSID: HTML5 WebAudio/JavaScript based C64 music emulator.
 	Copyright (C) 2011-2023 Juergen Wothke
-->
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="X-UA-Compatible" content="chrome=1" />
<title>webSID - stereo</title>

<meta name="description" content="This page illustrates the stereo features available in webSID.">
<meta name="keywords" content="webSID, Web Audio API, HTML5, JavaScript, C64, SID, music, Tiny'R'Sid, filter, emulation">
<meta name="author" content="Juergen Wothke">

<link rel="preload" href="stdlib/fonts/MaterialIcons-Regular-Stripped.woff2" as="font" crossOrigin="anonymous"/>

<link href="stdlib/websid_playlist.css" rel="stylesheet" type="text/css">

<link rel="image_src" href="screenshot.gif" />
<meta property="og:image" content="https://www.wothke.ch/websid_stereo/screenshot.gif" />

<link rel="icon" href="favicon.ico" type="image/x-icon">
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
<link type="image/x-icon" href="favicon.ico" />

<script>
	let allFilesLoadedCallback = function() {
		$(function() {	// DOM also is loaded
			try {
				new Main().run();
			}
			catch (e) {
				alert("error: your browser does not support ES6 'class' construct and you'll need a more recent browser to view this page." +e);
			}
		});
	}

	/*
	If HTML wasn't such a green banana joke, there would be a built-in feature to allow
	to specify a simple "include dependencies" configuration in a human readable form... but
	since this doesn't exist I am using https://github.com/muicss/loadjs as a poor man's
	crutch (I somewhat enhanced that API with loadJsBundles)
	*/
	function loadJsBundles(e){const n=function(e,n,t){let s=n[0];loadjs(s,e);let r=n[1];if(void 0!==r&&null!=r)if("function"==typeof r){let n=function(){r()};loadjs.ready(e,{success:n})}else{let n=function(){t(r)};loadjs.ready(e,{success:n})}};for(const[t,s]of Object.entries(e))if("@onReady"==t){let e=s[0],n=function(){loadJsBundles(s[1])};loadjs.ready(e,{success:n})}else n(t,s,loadJsBundles)}loadjs=function(){var e=function(){},n={},t={},s={};function r(e,n){if(e){var r=s[e];if(t[e]=n,r)for(;r.length;)r[0](e,n),r.splice(0,1)}}function o(n,t){n.call&&(n={success:n}),t.length?(n.error||e)(t):(n.success||e)(n)}function c(n,t,s,r){var o,i,u=document,l=s.async,f=(s.numRetries||0)+1,a=s.before||e,d=n.replace(/[\?|#].*$/,""),h=n.replace(/^(css|img)!/,"");r=r||0,/(^css!|\.css$)/.test(d)?((i=u.createElement("link")).rel="stylesheet",i.href=h,(o="hideFocus"in i)&&i.relList&&(o=0,i.rel="preload",i.as="style")):/(^img!|\.(png|gif|jpg|svg|webp)$)/.test(d)?(i=u.createElement("img")).src=h:((i=u.createElement("script")).src=n,i.async=void 0===l||l),!(i.onload=i.onerror=i.onbeforeload=function(e){var u=e.type[0];if(o)try{i.sheet.cssText.length||(u="e")}catch(e){18!=e.code&&(u="e")}if("e"==u){if((r+=1)<f)return c(n,t,s,r)}else if("preload"==i.rel&&"style"==i.as)return i.rel="stylesheet";t(n,u,e.defaultPrevented)})!==a(n,i)&&u.head.appendChild(i)}function i(e,t,s){var i,u;if(t&&t.trim&&(i=t),u=(i?s:t)||{},i){if(i in n)throw"LoadJS";n[i]=!0}function l(n,t){!function(e,n,t){var s,r,o=(e=e.push?e:[e]).length,i=o,u=[];for(s=function(e,t,s){if("e"==t&&u.push(e),"b"==t){if(!s)return;u.push(e)}--o||n(u)},r=0;r<i;r++)c(e[r],s,t)}(e,(function(e){o(u,e),n&&o({success:n,error:t},e),r(i,e)}),u)}if(u.returnPromise)return new Promise(l);l()}return i.ready=function(e,n){return function(e,n){e=e.push?e:[e];var r,o,c,i=[],u=e.length,l=u;for(r=function(e,t){t.length&&i.push(e),--l||n(i)};u--;)o=e[u],(c=t[o])?r(o,c):(s[o]=s[o]||[]).push(r)}(e,(function(e){o(n,e)})),i},i.done=function(e){r(e,[])},i.reset=function(){n={},t={},s={}},i.isDefined=function(e){return e in n},i}();

	loadJsBundles({
		jQueryBase: [[	'stdlib/jquery1.11.min.js'], {
								jQueryBundle: [[	'stdlib/jquery-ui1.10.2.min.js',
													'stdlib/jquery.details.min.js',
													'stdlib/playlist_widget.min.js'
													]],
						}],
		playerBase: [[	'stdlib/scriptprocessor_player.min.js'], {
								playerBundle: [[	'backend_websid.js',
													'stdlib/channelstreamer.min.js'
													]],
							}],
		"@onReady": [['jQueryBundle', 'playerBundle'], {
								mainBundle: [['main.js'], allFilesLoadedCallback]
							}]
	});

</script>
</head>


<body>
<div class="tooltip" id="tooltip" ></div>
<details role="main">
  <summary>What's this?</summary>
  <div>
  <h1>webSID stereo features</h1>
 <p>Copyright (C) 2011-2023 by Juergen Wothke  (The source code can be found <a href="https://bitbucket.org/wothke/websid" rel="noopener" target="_blank">here</a>.)</p>
 <p>This page illustrates webSID's stereo features. These features should NOT be activated if you are a purist 
 looking for the original C64/SID chip experience (in that case you probably don't like emulators anyway and should 
 better not be here to begin with): The original chip outputs *one* mono-audio-signal into which all the
 SID's three voices are mixed. The only means by which stereo can be produced with the original hardware is by wiring 
 up some non-standard multi-SID configuration.</p>
 
 <p>Unlike the original hardware, webSID provides voice level stereo panning: This allows to simulate the above multi-SID
 scenarios as well as single SID stereo not supported by the original SID chip. In addition webSID provides an add-on 
 "stereo enhancer" post-processing that may improve the listening experience via headphones. </p>

 <p>Usage: The "mode" dropdown is the main control used to select the used "stereo features". The "panning sliders" can 
 always be used except in the "no stereo" mode. Whereas the "headphone" and "reverb" controls only have
 an effect while in some "enhance" mode. 
 </p>
  <p>
  Note: The optional "enhance" modes require additional processing and increase webSID's "workload" - which 
  might be relevant for users of slower devices. (The closer the "workload" gets to 100% means that the
  audio-output may no longer get produced fast enough for realtime playback.)
 </p>
 
 <p>The webSID emulator allows to play original/binary C64 music files directly in 
 a web browser. (More information can be found on its
 <a ref="https://www.facebook.com/TinyRSid" target="_blank">Facebook page</a>.)</p> 
 </div>
</details>
<section>
<div id="mainContainer" class="mainRow">
	<div class="mainColumn1">
		<div class="playerRow">
			<div id="playerContainer" class="playerColumn"></div>
			<div class="playerColumn2"><img class="sidImage" src="6581.webp"></img></div>
		</div>
		<div class="spacerContainer"></div>
		<div id="scopesContainer"></div>
		<div>&nbsp;&nbsp;&nbsp;<span id="timing"></span></div>
	</div>
</div>

</section>

</body>
</html>
