# Generic WebAudio Node based player.
 
	version 1.2
	Copyright (C) 2015-2023 Juergen Wothke

	
This player relies on WebAudio Node (e.g. ScriptProcessorNode or AudioWorkletNode) generated 
sample data. It can be used whenever there is a respective Node that directly produces audio 
sample data. So far it has proved useful for porting different C/C++ based chiptune players to 
the Web (see my other projects).

In order to use this player with a new audio source you have to write a respective 
*BackendAdapter implementation. Unfortunately there isn't much documentation at the moment (except 
for the occasional comment present in the non-minified scriptprocessor_player.js). However my 
various chiptune players provide examples to learn from..


Features with regard to ScriptProcessorNode based backends are:

- "synchronous" file loading for backends that need to load additional files depending 
  on the loaded song (e.g. drumkits, player binaries, OS libraries, config files, etc) 
- support for backends that produce buffer sizes different from what WebAudio expects
- resampling for backends that use a sampleRate different from what WebAudio expects
  (or to tweak the used playback speed)
- stereo panning; mono to stereo
- startup silence skipping
- playtime timeout and song end detection
- the player can be reconfigured at runtime to deal with different audio 
  sources (see http://www.wothke.ch/blaster/)


Originally the player had been designed for use of ScriptProcessorNode based implementations and
a large part of the code is still tailored to that scenario (see ScriptNodeBackendAdapter and its
sub-classes). 

Starting with version 1.2 the original implementaion was significantly refactored
to also allow for the use of AudioWorkletNode based implementations. The respective
AudioWorklet specific implemenations currently are experimental and DO NOT cover the complete 
functionality available for the ScriptProcessorNode scenario. (for more background information
I'd suggest you read my findings here: https://jwothke.wordpress.com/2023/10/12/why-you-should-not-use-audioworklet/ )
Also the interfaces where switched from "poor man's" fake JavaScript classes to regular ES6 classes.
This means that older backends that were designed for the old player no longer work with the 
new player. (You'll have to use an older version < 1.2 to use those. I updated all by backends
to work with the new player version.) Speaking of older versions: If your old backends use WASM
then you'll need a player version >=1.02 to use them.


Note: ES6 "private" fields/methods were only introduced quite recently on certain platforms (e.g. 
in 2021 for iOS Safari). Though it would be nice to use respective ES6 features from a software 
enigneering perspective I value my user's freedom to chose what hardware/browser they want to use 
more highly: Consequently I am not using that feature yet - so the player should still work 
even on old iPads.

	
## Add-on

The channelstreamer.js file implements the player's AbstractTicker API and can be used to 
visualize player internal data streams (e.g. different music tracker channels) in sync 
with the actual WebAudio playback. An example that use this can be found 
here: https://www.wothke.ch/webV2M/
 
	
## License	
	Terms of Use: This software is licensed under a CC BY-NC-SA 
	(http://creativecommons.org/licenses/by-nc-sa/4.0/).
